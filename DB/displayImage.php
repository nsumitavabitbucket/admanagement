<?php
define('DB_SERVER', 'localhost');
   define('DB_USERNAME', 'root');
   define('DB_PASSWORD', '');
   define('DB_DATABASE', 'admanagement');
   $db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE) or die("Connection failed: " . mysqli_connect_error());
	function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}

	/*$currentDate=date("Y-m-d h:i:s");	
	$clientId=30;
	$camp_Id=3;
	$query = mysqli_query($db,"SELECT creative,target_url FROM `campaign` WHERE c_client_id='$clientId' AND c_id='$camp_Id' AND '$currentDate' BETWEEN date_start AND date_end");		
	$row = mysqli_fetch_array($query);
    
	$image='http://localhost/wethink/event//assets/uploads/'.$row['creative'];
	$targetUrl=$row['target_url'];
	
	$response[] = array("targetUrl" => $targetUrl,
						 "image" => $image);  
	$jsonRes = json_encode($response);	
	echo $jsonRes;*/
	
	if(!isset($_GET['Clicked'])){
    // if the Clicked parameter is not set, we came to the page normally

    // Let's select a random banner from the database
    $result = mysqli_query($db,"SELECT * FROM campaign ORDER BY RAND() LIMIT 1") or die(mysqli_error($db));
    $row = mysqli_fetch_array($result);   

    // Now let's increment the Views field for the banner we selected
    mysqli_query($db,"UPDATE campaign SET views = views + 1 WHERE c_id = '" . $row['c_id'] . "'") or die(mysqli_error());

    // let's set the URL to this same page but with the Clicked parameter set
    $url = "displayImage.php?Clicked=" . $row['c_id'];

    // Last but not least, we have to output the banner HTML
    // Notice, I set the caption on both the 'alt' and 'title' attributes of the 'img' tag,
    // that's because IE shows the value of the 'alt' tag when an image is hovered,
    // whereas Firefox shows the value of the 'title' tag, just thought you might want that!
    echo "<a href=\"" . $url . "\" onclick=\"display(".ip_info('Visitor','Country').");\"><img src=\"http://localhost/wethink/event//assets/uploads/" . $row['creative'] . "\" alt=\"" . $row['creative'] . "\" title=\"" . $row['creative'] . "\" /></a>";

}else{
	
    // Otherwise, increment the Clicks field and redirect

    // First, let's get the ID of the banner that was clicked from the Clicked parameter
    $id = (int)$_GET['Clicked'];

    // now let's increment the Clicks field for the banner
    mysqli_query($db,"UPDATE campaign SET Clicks = Clicks + 1 WHERE c_id = '" . $id . "'") or die(mysqli_error());

    // now let's select the banner from the database so we can redirect to the URL that's associated with it
    $result = mysqli_query($db,"SELECT * FROM campaign WHERE c_id = '" . $id . "'") or die(mysqli_error());
    $row = mysqli_fetch_array($result);

    // redirect to the URL
    header("location: " . $row['target_url']);
}


// Close the MySql connection
mysqli_close($db);


?>