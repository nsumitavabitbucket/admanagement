-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2020 at 06:56 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `events`
--

-- --------------------------------------------------------

--
-- Table structure for table `2020_aws_dochin_registrations`
--

CREATE TABLE `2020_aws_dochin_registrations` (
  `idregistrations` int(5) UNSIGNED NOT NULL,
  `client_id` int(5) UNSIGNED NOT NULL,
  `event_id` int(5) UNSIGNED NOT NULL,
  `day_id` int(5) UNSIGNED NOT NULL,
  `shift_id` int(5) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `mob_number` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `job_role` varchar(100) NOT NULL,
  `year` varchar(10) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `registration_type` varchar(100) NOT NULL COMMENT '1=onsite,0=pre_register',
  `company_size` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `2020_aws_eeochin_registrations`
--

CREATE TABLE `2020_aws_eeochin_registrations` (
  `idregistrations` int(5) UNSIGNED NOT NULL,
  `client_id` int(5) UNSIGNED NOT NULL,
  `event_id` int(5) UNSIGNED NOT NULL,
  `day_id` int(5) UNSIGNED NOT NULL,
  `shift_id` int(5) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `mob_number` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `job_role` varchar(100) NOT NULL,
  `year` varchar(10) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `registration_type` varchar(100) NOT NULL COMMENT '1=onsite,0=pre_register',
  `company_size` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `2020_aws_jaipur_registrations`
--

CREATE TABLE `2020_aws_jaipur_registrations` (
  `idregistrations` int(5) UNSIGNED NOT NULL,
  `client_id` int(5) UNSIGNED NOT NULL,
  `event_id` int(5) UNSIGNED NOT NULL,
  `day_id` int(5) UNSIGNED NOT NULL,
  `shift_id` int(5) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `mob_number` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `job_role` varchar(100) NOT NULL,
  `year` varchar(10) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `registration_type` varchar(100) NOT NULL COMMENT '1=onsite,0=pre_register',
  `company_size` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `2020_aws_kerala_registrations`
--

CREATE TABLE `2020_aws_kerala_registrations` (
  `idregistrations` int(5) UNSIGNED NOT NULL,
  `client_id` int(5) UNSIGNED NOT NULL,
  `event_id` int(5) UNSIGNED NOT NULL,
  `day_id` int(5) UNSIGNED NOT NULL,
  `shift_id` int(5) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `mob_number` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `job_role` varchar(100) NOT NULL,
  `year` varchar(10) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `registration_type` varchar(100) NOT NULL COMMENT '1=onsite,0=pre_register',
  `company_size` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `2020_aws_kochin_registrations`
--

CREATE TABLE `2020_aws_kochin_registrations` (
  `idregistrations` int(5) UNSIGNED NOT NULL,
  `client_id` int(5) UNSIGNED NOT NULL,
  `event_id` int(5) UNSIGNED NOT NULL,
  `day_id` int(5) UNSIGNED NOT NULL,
  `shift_id` int(5) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `mob_number` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `job_role` varchar(100) NOT NULL,
  `year` varchar(10) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `registration_type` varchar(100) NOT NULL COMMENT '1=onsite,0=pre_register',
  `company_size` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `2020_aws_mochin_registrations`
--

CREATE TABLE `2020_aws_mochin_registrations` (
  `idregistrations` int(5) UNSIGNED NOT NULL,
  `client_id` int(5) UNSIGNED NOT NULL,
  `event_id` int(5) UNSIGNED NOT NULL,
  `day_id` int(5) UNSIGNED NOT NULL,
  `shift_id` int(5) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `mob_number` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `job_role` varchar(100) NOT NULL,
  `year` varchar(10) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `registration_type` varchar(100) NOT NULL COMMENT '1=onsite,0=pre_register',
  `company_size` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `2020_aws_vizag_registrations`
--

CREATE TABLE `2020_aws_vizag_registrations` (
  `idregistrations` int(5) UNSIGNED NOT NULL,
  `client_id` int(5) UNSIGNED NOT NULL,
  `event_id` int(5) UNSIGNED NOT NULL,
  `day_id` int(5) UNSIGNED NOT NULL,
  `shift_id` int(5) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `mob_number` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `job_role` varchar(100) NOT NULL,
  `year` varchar(10) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `registration_type` varchar(100) NOT NULL COMMENT '1=onsite,0=pre_register',
  `company_size` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `2020_aws_wochin_registrations`
--

CREATE TABLE `2020_aws_wochin_registrations` (
  `idregistrations` int(5) UNSIGNED NOT NULL,
  `client_id` int(5) UNSIGNED NOT NULL,
  `event_id` int(5) UNSIGNED NOT NULL,
  `day_id` int(5) UNSIGNED NOT NULL,
  `shift_id` int(5) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `mob_number` varchar(100) NOT NULL,
  `pincode` varchar(30) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `industry` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `job_role` varchar(100) NOT NULL,
  `year` varchar(10) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `registration_type` varchar(100) NOT NULL COMMENT '1=onsite,0=pre_register',
  `company_size` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(15) NOT NULL DEFAULT 'event',
  `event` int(100) NOT NULL COMMENT 'event_id',
  `status` enum('Active','None') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `name`, `username`, `password`, `level`, `event`, `status`) VALUES
(26, 'Admin', 'admin', 'admin', 'Super', 0, 'Active'),
(31, 'Shaniba', 'awsAhmedabad', 'zaq123', 'event', 11, 'Active'),
(28, 'Web Team', 'super admin', '1234', 'Super', 10, 'Active'),
(29, 'Shaniba', 'shanusts', '123', 'event', 10, 'Active'),
(30, 'Shaniba', 'adm', '1234', 'event', 11, 'Active'),
(32, 'Danny', 'don', '1234', 'event', 12, 'Active'),
(33, 'Shaniba', 'Shan', '123', 'event', 13, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE `client_info` (
  `idclient_info` int(11) NOT NULL,
  `client_info_client_name` varchar(100) DEFAULT NULL,
  `client_info_logo` varchar(200) NOT NULL,
  `client_info_company_name` varchar(100) DEFAULT NULL,
  `client_info_email` varchar(100) DEFAULT NULL,
  `client_info_mobile` varchar(100) DEFAULT NULL,
  `client_info_address` varchar(100) DEFAULT NULL,
  `client_info_status` varchar(45) DEFAULT NULL COMMENT '1=active 2=inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`idclient_info`, `client_info_client_name`, `client_info_logo`, `client_info_company_name`, `client_info_email`, `client_info_mobile`, `client_info_address`, `client_info_status`) VALUES
(2, 'Amazone', '4c011-aws_logo_179x109.gif', 'AWS', 'info@aws.com', '9605017253', 'Address', '1');

-- --------------------------------------------------------

--
-- Table structure for table `day_master`
--

CREATE TABLE `day_master` (
  `idday_master` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `day_list` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `day_master`
--

INSERT INTO `day_master` (`idday_master`, `event_id`, `day_list`) VALUES
(1, 13, '2020-02-18'),
(2, 13, '2020-02-19'),
(10, 46, '2020-02-19'),
(11, 46, '2020-02-20'),
(12, 46, '2020-02-21'),
(13, 47, '2020-03-24'),
(14, 47, '2020-03-25');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `template_name` enum('contact_us','registration') COLLATE utf8_unicode_ci NOT NULL,
  `template_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `template_name`, `template_subject`, `content`, `created`, `status`) VALUES
(1, 'contact_us', 'wqewqe', '<p>\n	eqweqw</p>\n', '2020-03-11 00:00:00', '0'),
(2, 'contact_us', 'rererer', '<p>\n	&nbsp;</p>\n<p>\n	Dear&nbsp;&nbsp;{first_name},</p>\n<p>\n	Giving me&nbsp;&nbsp;{user_contact}</p>\n', '2020-03-18 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `idevents` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `event_name` varchar(100) DEFAULT NULL,
  `logo` varchar(200) NOT NULL,
  `event_desc` varchar(250) DEFAULT NULL,
  `event_start_date` date DEFAULT NULL,
  `event_end_date` date DEFAULT NULL,
  `event_status` int(11) DEFAULT NULL COMMENT '1=active,2=deactive',
  `event_location` varchar(100) DEFAULT NULL,
  `event_city` varchar(100) DEFAULT NULL,
  `admin_color` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `import_csv` enum('yes','no') NOT NULL,
  `print_qr` enum('yes','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`idevents`, `client_id`, `event_name`, `logo`, `event_desc`, `event_start_date`, `event_end_date`, `event_status`, `event_location`, `event_city`, `admin_color`, `url`, `import_csv`, `print_qr`) VALUES
(13, 2, 'AWS Jaipur', '4c011-aws_logo_179x109.gif', 'Jaipur', '2020-02-17', '2020-02-19', 1, 'Mumbai', 'Mumbai', '#eb4034', 'Aws_Jaipur', 'yes', 'yes'),
(46, 2, 'Google Partner Summit 2018', 'ed07c-email_sig_3-22.png', 'qwweqw', '2020-02-19', '2020-02-21', 1, 'Mumbai', 'hfghfgh', 'green', 'AWS_WW', 'yes', 'yes'),
(47, 2, 'AWS Vizag', '', NULL, '2020-03-24', '2020-03-25', 1, 'Vizag', 'Vizag', 'green', 'AWS_Vizag', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `event_agenda`
--

CREATE TABLE `event_agenda` (
  `agenda_id` int(11) NOT NULL,
  `eventids` int(11) NOT NULL,
  `event_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `hall_id` int(11) NOT NULL,
  `session` varchar(200) NOT NULL,
  `speaker_fid` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_agenda`
--

INSERT INTO `event_agenda` (`agenda_id`, `eventids`, `event_date`, `start_time`, `end_time`, `hall_id`, `session`, `speaker_fid`, `created`) VALUES
(1, 13, '2020-03-06', '05:11:00', '16:50:00', 1, 'Aiming Up: The Dreams and the Realities of Start Ups', 1, '2020-03-06 11:12:24'),
(2, 13, '2020-03-06', '16:33:00', '16:59:00', 1, 'Test Session 2', 1, '2020-03-06 11:15:23'),
(3, 13, '2020-03-06', '16:59:00', '17:09:00', 1, 'Test Session 3', 1, '2020-03-06 11:15:27'),
(4, 47, '2020-03-24', '01:30:00', '02:30:00', 1, 'Core Java', 0, '2020-03-12 08:51:25'),
(5, 47, '2020-03-25', '01:30:00', '02:30:00', 1, 'Session 1: Tea', 0, '0000-00-00 00:00:00'),
(6, 46, '2020-02-19', '02:30:00', '03:30:00', 1, '2158370825', 0, '2020-03-12 10:29:01');

-- --------------------------------------------------------

--
-- Table structure for table `event_day_details`
--

CREATE TABLE `event_day_details` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `day_list` varchar(100) NOT NULL,
  `session` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_day_details`
--

INSERT INTO `event_day_details` (`id`, `event_id`, `day_list`, `session`) VALUES
(1, 1, '', 'cxo'),
(15, 13, '2020-02-17', ''),
(16, 13, '2020-02-18', '2158370825'),
(17, 13, '2020-02-19', '45354');

-- --------------------------------------------------------

--
-- Table structure for table `event_rooms`
--

CREATE TABLE `event_rooms` (
  `idrooms` int(11) NOT NULL,
  `hall` varchar(20) NOT NULL,
  `hall_short_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_rooms`
--

INSERT INTO `event_rooms` (`idrooms`, `hall`, `hall_short_name`) VALUES
(1, 'Hall 1', 'HL1'),
(2, 'Hall 2', 'HL2');

-- --------------------------------------------------------

--
-- Table structure for table `event_speakers`
--

CREATE TABLE `event_speakers` (
  `speaker_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `comapny_name` varchar(50) NOT NULL,
  `created_d` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_speakers`
--

INSERT INTO `event_speakers` (`speaker_id`, `event_id`, `name`, `designation`, `comapny_name`, `created_d`) VALUES
(1, 13, 'Shaniba Shanif', 'Developer', 'Neoniche', '2020-02-18 09:58:17');

-- --------------------------------------------------------

--
-- Table structure for table `registrations`
--

CREATE TABLE `registrations` (
  `idregistrations` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `day_id` date DEFAULT NULL,
  `shift_id` varchar(30) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `mob_number` varchar(45) DEFAULT NULL,
  `pincode` varchar(45) DEFAULT NULL,
  `company_type` varchar(100) DEFAULT NULL,
  `industry` varchar(100) DEFAULT NULL,
  `designation` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `job_role` varchar(45) DEFAULT NULL,
  `company_size` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  `user_type` varchar(45) DEFAULT NULL,
  `registration_type` varchar(45) DEFAULT NULL COMMENT 'onsite,pre_register, etc',
  `registration_status` int(11) DEFAULT NULL COMMENT '1=Confirmed 2=Pending',
  `uniq_code` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrations`
--

INSERT INTO `registrations` (`idregistrations`, `client_id`, `event_id`, `day_id`, `shift_id`, `full_name`, `company_name`, `mob_number`, `pincode`, `company_type`, `industry`, `designation`, `email`, `city`, `job_role`, `company_size`, `created`, `year`, `user_type`, `registration_type`, `registration_status`, `uniq_code`) VALUES
(1, 1, 13, '2020-02-18', 'cxo', 'Shaniba', 'Reddislab', '9884609689', '680303', 'type', 'IT', 'designation', 'email@gmail.com', 'TCR', 'Developer', '100', '2020-01-06 05:15:00', '1993', 'Delegate', '0', 0, ''),
(2, 1, 13, '2020-02-18', 'crt', 'Shanif', 'DSP', '6469450628', '680303', 'type', 'Construction', 'Designation', 'email@gmail.com', 'Mumbai', 'Designer', '100', '2020-01-06 05:15:00', '2020', 'Delegate', '0', 0, ''),
(3, 1, 13, '2020-02-18', 'cxo', 'Hafsa', 'DSP', '6469450628', '680303', 'type', 'Construction', 'Designation', 'email@gmail.com', 'Mumbai', 'Designer', '100', '2020-01-06 05:15:00', '2020', 'Delegate', '0', 0, ''),
(4, 1, 13, '2020-02-18', 'crt', 'Manoj', 'Neoniche', '6469450628', '680303', 'type', 'Construction', 'Designation', 'email@gmail.com', 'Mumbai', 'Designer', '100', '2020-01-06 05:15:00', '2020', 'Delegate', '0', 1, ''),
(5, 1, 13, '2020-02-19', '-', 'Danny', 'Neoniche', '6469450628', '680303', 'type', 'Construction', 'Designation', 'email@gmail.com', 'Mumbai', 'Designer', '100', '2020-01-06 05:15:00', '2020', 'Delegate', '0', 1, '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `sessions`
--
CREATE TABLE `sessions` (
`id` int(11)
,`event_id` int(11)
,`day_list` varchar(100)
,`session` varchar(100)
,`reg_links` varchar(201)
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Primary Key',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `email` varchar(255) NOT NULL COMMENT 'Email Address',
  `contact_no` varchar(50) NOT NULL COMMENT 'Contact No',
  `created_at` varchar(20) NOT NULL COMMENT 'Created date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='datatable demo table';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `contact_no`, `created_at`) VALUES
(1, 'Team', 'info@test.com', '9000000001', '2019-01-01'),
(2, 'Admin', 'admin@test.com', '9000000002', '2019-02-01'),
(3, 'User', 'user@test.com', '9000000003', '2018-03-01'),
(4, 'Editor', 'editor@test.com', '9000000004', '2019-04-01'),
(5, 'Writer', 'writer@test.com', '9000000005', '2017-05-01'),
(6, 'Contact', 'contact@test.com', '9000000006', '2019-06-01'),
(7, 'Manager', 'manager@test.com', '9000000007', '2019-07-01'),
(8, 'John', 'john@test.com', '9000000055', '2016-08-01'),
(9, 'Merry', 'merry@test.com', '9000000088', '2019-09-01'),
(10, 'Keliv', 'kelvin@test.com', '9000550088', '2019-11-01'),
(11, 'Herry', 'herry@test.com', '9050550088', '2019-11-01'),
(12, 'Mark', 'mark@test.com', '9050550998', '2019-12-01');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `iduser_type` int(11) NOT NULL,
  `user_type` varchar(45) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `sessions`
--
DROP TABLE IF EXISTS `sessions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sessions`  AS  select `event_day_details`.`id` AS `id`,`event_day_details`.`event_id` AS `event_id`,`event_day_details`.`day_list` AS `day_list`,`event_day_details`.`session` AS `session`,(case when (`event_day_details`.`session` <> '') then concat(`event_day_details`.`day_list`,'/',`event_day_details`.`session`) else `event_day_details`.`day_list` end) AS `reg_links` from `event_day_details` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `2020_aws_dochin_registrations`
--
ALTER TABLE `2020_aws_dochin_registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `2020_aws_eeochin_registrations`
--
ALTER TABLE `2020_aws_eeochin_registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `2020_aws_jaipur_registrations`
--
ALTER TABLE `2020_aws_jaipur_registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `2020_aws_kerala_registrations`
--
ALTER TABLE `2020_aws_kerala_registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `2020_aws_kochin_registrations`
--
ALTER TABLE `2020_aws_kochin_registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `2020_aws_mochin_registrations`
--
ALTER TABLE `2020_aws_mochin_registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `2020_aws_vizag_registrations`
--
ALTER TABLE `2020_aws_vizag_registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `2020_aws_wochin_registrations`
--
ALTER TABLE `2020_aws_wochin_registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
  ADD PRIMARY KEY (`idclient_info`);

--
-- Indexes for table `day_master`
--
ALTER TABLE `day_master`
  ADD PRIMARY KEY (`idday_master`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`idevents`);

--
-- Indexes for table `event_agenda`
--
ALTER TABLE `event_agenda`
  ADD PRIMARY KEY (`agenda_id`);

--
-- Indexes for table `event_day_details`
--
ALTER TABLE `event_day_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_rooms`
--
ALTER TABLE `event_rooms`
  ADD PRIMARY KEY (`idrooms`);

--
-- Indexes for table `event_speakers`
--
ALTER TABLE `event_speakers`
  ADD PRIMARY KEY (`speaker_id`);

--
-- Indexes for table `registrations`
--
ALTER TABLE `registrations`
  ADD PRIMARY KEY (`idregistrations`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`iduser_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `2020_aws_dochin_registrations`
--
ALTER TABLE `2020_aws_dochin_registrations`
  MODIFY `idregistrations` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `2020_aws_eeochin_registrations`
--
ALTER TABLE `2020_aws_eeochin_registrations`
  MODIFY `idregistrations` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `2020_aws_jaipur_registrations`
--
ALTER TABLE `2020_aws_jaipur_registrations`
  MODIFY `idregistrations` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `2020_aws_kerala_registrations`
--
ALTER TABLE `2020_aws_kerala_registrations`
  MODIFY `idregistrations` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `2020_aws_kochin_registrations`
--
ALTER TABLE `2020_aws_kochin_registrations`
  MODIFY `idregistrations` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `2020_aws_mochin_registrations`
--
ALTER TABLE `2020_aws_mochin_registrations`
  MODIFY `idregistrations` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `2020_aws_vizag_registrations`
--
ALTER TABLE `2020_aws_vizag_registrations`
  MODIFY `idregistrations` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `2020_aws_wochin_registrations`
--
ALTER TABLE `2020_aws_wochin_registrations`
  MODIFY `idregistrations` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `client_info`
--
ALTER TABLE `client_info`
  MODIFY `idclient_info` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `day_master`
--
ALTER TABLE `day_master`
  MODIFY `idday_master` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `idevents` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `event_agenda`
--
ALTER TABLE `event_agenda`
  MODIFY `agenda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `event_day_details`
--
ALTER TABLE `event_day_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `event_rooms`
--
ALTER TABLE `event_rooms`
  MODIFY `idrooms` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `event_speakers`
--
ALTER TABLE `event_speakers`
  MODIFY `speaker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `registrations`
--
ALTER TABLE `registrations`
  MODIFY `idregistrations` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
