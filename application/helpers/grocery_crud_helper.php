<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include(APPPATH . 'libraries/GroceryCrudEnterprise/autoload.php');
use GroceryCrud\Core\GroceryCrud;
if (!function_exists('_getDbData')) {
 function _getDbData() {
        $db = [];
        include(APPPATH . 'config/database.php');
        return [
            'adapter' => [
                'driver' => 'Pdo_Mysql',
                'host'     => $db['default']['hostname'],
                'database' => $db['default']['database'],
                'username' => $db['default']['username'],
                'password' => $db['default']['password'],
                'charset' => 'utf8'
            ]
        ];
    }
}

if (!function_exists('_getGroceryCrudEnterprise')) {
function _getGroceryCrudEnterprise($bootstrap = true, $jquery = true) {
        $db = _getDbData();
        $config = include(APPPATH . 'config/gcrud-enteprise.php');
        $groceryCrud = new GroceryCrud($config, $db);
        return $groceryCrud;
}
}

function _example_output($output = null) {
	 $ci=& get_instance();
    if (isset($output->isJSONResponse) && $output->isJSONResponse) {
                header('Content-Type: application/json; charset=utf-8');
                echo $output->output;
                exit;
    }

    $ci->load->view('Gc_view.php',$output);    
}

?>