<?php

function create_new_page($tablename, $class_name, $controller_name){

  // Create Controller
  $controller = fopen(APPPATH.'controllers/admin/'.$controller_name.'.php', "a")
  or die("Unable to open file!");

  $controller_content ="<?php defined('BASEPATH') OR exit('No direct script access allowed');
  
  class $class_name extends CI_Controller  {
  
public function __construct()
    {
       \$data=array();
		parent::__construct();
        
        \$this->table_name='".$tablename."';
        \$this->load->helper('grocery_crud_helper');
        \$this->load->model('Login_auth_db');
        \$this->load->model('registrationModel');
        \$this->load->library('form_validation');
        \$this->load->library('Grocery_CRUD');
        \$this->className=\$this->router->fetch_class();
        
    }
    public function index()
    {
        
        \$data['eventDetails']=getEvent('id',\$this->router->fetch_class());
      
        if(\$this->input->post('username'))
		{
          
            // set validation rules
            \$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
            \$this->form_validation->set_rules('password', 'Password', 'required');
            if (\$this->form_validation->run() == true) {

            	// set variables from the form
			\$username = \$this->input->post('username');
			\$password = \$this->input->post('password');
			
			if (\$this->Login_auth_db->login(\$username,\$password)) {
				\$user_data = \$this->Login_auth_db->get_user_data(\$username);

                if(\$user_data['level']=='event')
                {
                    \$checkuserforthisevent=checkuserRegisteredForthisEvent(\$this->className,\$user_data['event']);
                    if(count(\$checkuserforthisevent>0))
                    {
                       \$this->Login_auth_db->setSession(\$user_data); 
                    }else
                    {
                      \$data['error'] = 'You are not a valid user for this event.';  
                    }
                    
                }else
                {
                     \$this->Login_auth_db->setSession(\$user_data); 
                }

				
				redirect('admin/'.\$this->className.'/home');
				
			} else {
				
				// login failed
				 \$data['error'] = 'Wrong username or password.';
            }

            }           
        
        }
        
       \$this->load->view('admin/login',\$data);
        
        
        
    }
    
   
    
    public function home()
    {
        
        \$data['content'] = getEvent('id',\$this->router->fetch_class())['event_name'] .'dashboard';
		\$data['count']['ContactEnq']=1;
		\$data['count']['expo']=2;
		\$data['count']['sponsorship']=3;
		\$data['count']['stayupdated']=4;
        \$this->load->view('event_admin/event_admin_dashboard',$data);
    }
    
     public function onsiteRegistrations()
    {
         \$data['result']=getOnsitelist(\$this->className);
         \$this->load->view('event_admin/event_onsite_links',\$data);
         
    }
    
  
    public function registrations(\$type,\$day,\$id='',\$session='')
    {
     
        
          if(\$type==='pre')
            {
                \$typeid=0;
            }
            if(\$type==='onsite')
            {
                \$typeid=1;
            }
        
            \$eventID=getEvent('id',\$this->className)['idevents']; 
        
try{
     \$crud = _getGroceryCrudEnterprise();
    \$crud->setTable('registrations');
    \$crud->setSubject('Registrations');
   \$crud->columns(['company_name','full_name','mob_number','uniq_code','user_type','registration_status']);
   \$crud->fieldType('registration_type','dropdown',array('0' => 'Pre', '1' => 'Onsite'));
    \$crud->fieldType('registration_status','dropdown',array('0' => 'Pending', '1' => 'Confirmed'));
\$crud->unsetAdd();

    \$crud->where(['day_id = ?' => \$day,'registration_type = ?' => \$typeid,'event_id = ?' => \$eventID]);
        if(!empty(\$session))
    {
        \$crud->where(['shift_id = ?' => \$session]);

    }

       
    \$output = \$crud->render();
    
    \$data['title']=ucwords(\$type).'Registrations '.\$day.' '.\$session;

    \$output->data = \$data;
    _example_output(\$output);

    }catch(Exception \$e){
      show_error(\$e->getMessage().' --- '.\$e->getTraceAsString());
    }
        
         
    }
  
   public function attendee(\$type,\$day,\$id='',\$session='')
    {
     
        
          if(\$type==='pre')
            {
                \$typeid=0;
            }
            if(\$type==='onsite')
            {
                \$typeid=1;
            }
        
            \$eventID=getEvent('id',\$this->className)['idevents']; 
        
try{
     \$crud = _getGroceryCrudEnterprise();
    \$crud->setTable('registrations');
    \$crud->setSubject('Registrations');
   \$crud->columns(['company_name','full_name','mob_number','uniq_code','user_type','registration_status']);
    \$crud->fieldType('registration_type','dropdown',array('0' => 'Pre', '1' => 'Onsite'));
    \$crud->fieldType('registration_status','dropdown',array('0' => 'Pending', '1' => 'Confirmed'));
          
\$crud->unsetAdd();

    \$crud->where(['day_id = ?' => \$day,'registration_type = ?' => \$typeid,'event_id = ?' => \$eventID,'registration_status = ?' => 1]);
        if(!empty(\$session))
    {
        \$crud->where(['shift_id = ?' => \$session]);

    }

       
    \$output = \$crud->render();
    \$data['title']=ucwords(\$type).'Registrations '.\$day.' '.\$session.' Attendees';

    \$output->data = \$data;
    _example_output(\$output);

    }catch(Exception \$e){
      show_error(\$e->getMessage().' --- '.\$e->getTraceAsString());
    }
        
         
    }
  
  }?>";
    
    
    
    
    
  fwrite($controller, "\n". $controller_content);
  fclose($controller);

 
   }

   function getDatesFromRange($start, $end, $format = 'Y-m-d') { 
      
    // Declare an empty array 
    $array = array(); 
      
    // Variable that store the date interval 
    // of period 1 day 
    $interval = new DateInterval('P1D'); 
  
    $realEnd = new DateTime($end); 
    $realEnd->add($interval); 
  
    $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
  
    // Use loop to store date into array 
    foreach($period as $date) {                  
        $array[] = $date->format($format);  
    } 
  
    // Return the array elements 
    return $array; 
} 
?>