<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class DigitalAgendaModel extends CI_Model
{ 
    function __construct() 
    { 
        // Set table name 
        //$this->table = 'iff_enquiry_user_details'; 
    } 
     public function getEventClients()
     {
         $this->db->select('*')
             ->from('client_info')
             ->where('client_info_status',1);
        $result=$this->db->get()->result();
        return $result;
     }  
    //Get All Active events As per client ID
    
    public function getClientsEvents($clientID)
     {
         $this->db->select('*')
             ->from('events')
             ->where('event_status',1);
        $result=$this->db->get()->result();
        return $result;
     }
      //Get All Active events As per event ID
    
    public function getRoomsEvents($eventID)
     {
        //SELECT * FROM `event_rooms` inner join event_agenda on `idrooms`=event_agenda.hall_id GROUP by idrooms where event_agenda.event_id
         $this->db->select('*')
             ->from('event_rooms')
            ->join('event_agenda','idrooms=hall_id')
             ->where('eventids',$eventID)
            ->group_by('idrooms');
        $result=$this->db->get()->result();
        return $result;
     }  
    public function getNowAndNextSession($eventID,$roomID,$time,$today)
     {
         $this->db->select('*')
             ->from('event_agenda')
             ->where('eventids',$eventID)
             ->where('hall_id',$roomID)
             ->where('end_time >', $time) 
             ->where('event_date',$today)
            ->order_by('start_time','asc')
            ->limit(2);
        $result=$this->db->get()->result();
        //echo $this->db->last_query();//
        return $result;
     }
     
}
     
     ?>