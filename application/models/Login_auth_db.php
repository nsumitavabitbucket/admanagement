<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class Login_auth_db extends CI_Model {
	
	public function __construct() {
		
		parent::__construct();
				
	}
	
	function login($username, $password) 
	{
		$this->db->select('*');
		$this->db->from('admin_users');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->where('status', 'Active');
		$query = $this->db->get()->row();
		if(!empty($query)) 
		{
		return true;
		} else 
		{
		return false;
		}
		
	}
	
	function get_user_data($username) 
	{
		$this->db->select('*');
		$this->db->from('admin_users');
		$this->db->where('username', $username);	
		$query = $this->db->get();
		$user_data = array(
			'username'	=> $query->row('username'),
			'id'		=> $query->row('id'),
			'status'	=> $query->row('status'),
			'name'		=> $query->row('name'),
			'level'		=> $query->row('level'),
			'client_id'	=> $query->row('client_id'));
		return $user_data;
		
	}
     public function setSession($user_data)
    {
        $user_detail = array(
					'name'	=> $user_data['name'],
					'id'	=> $user_data['id'],
					'username'	=> $user_data['username'],
					'level'	=> $user_data['level'],
					'status'	=> $user_data['status'],
					'client_id'	=> $user_data['client_id']
				);
				$this->session->set_userdata($user_detail);
				$this->session->set_userdata('logged_in', true);
    }
	
	function change_pass($session_id,$new_pass)
	{
	$update_pass=$this->db->query("UPDATE admin_users set password='$new_pass'  where id='$session_id'");
	}
		
}