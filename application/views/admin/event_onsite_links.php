<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=getEvent('id',$this->router->fetch_class())['event_name'];?> Admin Panel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/css/bootstrap.min.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?=base_url();?>assets/admin/img/favicon.ico">
    <!-- Font Awesome CDN-->
<script src="https://use.fontawesome.com/99347ac47f.js"></script>
  </head>
  <body>
    <div class="page home-page">
      <!-- Main Navbar-->
	  <?php $this->load->view('layout/event_admin_header'); ?>

      <!-- Main Navbar-->
      
      <div class="page-content d-flex align-items-stretch">
        <!-- Side Navbar --> 
		<?php $this->load->view('layout/event_admin_side_bar'); ?>
		<!-- Side Navbar -->
	   
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Onsite Registrations Links</h2>
            </div>
          </header>

		
		<section class="tables">   
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-12">
                  <div class="card">

                     <!--  <div class="card-header d-flex align-items-center">
                   <h3 class="h4">Basic Table</h3>
                    </div>-->
                    <div class="card-body">
                     
				
<table class="table">
    <thead>
      <tr>
        <th>Day</th>
        <th>Session</th>
        <th>User Onsite RegLink</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach($result as $value){?>
      <tr>
        <td><?=$value->day_list;?></td>
        <td><?=empty($value->session)?'-':$value->session;?></td>
        <td><a href="<?=base_url();?>onsite/event/<?=$this->router->fetch_class();?>/<?=$value->day_list;if(!empty($value->id)){?>/<?=$value->id;?>/<?=$value->session;}?>" target="_blank"><?=$this->router->fetch_class();?>/<?=$value->day_list;if(!empty($value->id)){?>/<?=$value->session;}?></a></td>
      </tr>
        <?php } ?>
    </tbody>
 
</table>
					 
                    </div>
                  </div>
                </div>
                  </div> 
        </div>
        </section>

		
      </div>
    </div>
</div>
      <?php $this->load->view('layout/admin_footer'); ?>
  </body>
</html>