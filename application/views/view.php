<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?=getEvent('id',$this->router->fetch_class())['event_name'];?></title>
<?php $this->load->view('layout/event_admin/css')?>
     <?php
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
</head>
<body>
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <?php $this->load->view('layout/event_admin/sidebar-menu')?>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <?php $this->load->view('layout/event_admin/logo-header')?>
            <!-- page title area start -->
            <!-- <div class="page-title-area">
  <div class="row align-items-center">
    <div class="col-sm-6">
      <div class="breadcrumbs-area clearfix">
        <h4 class="page-title pull-left"><?php echo $data['title'];?></h4>
     
      </div>
    </div>
 
<?php //$this->load->view('layout/event_admin/logout')?>

  </div>
</div> -->
            <!-- page title area end -->
            <div class="main-content-inner">
               
<div style="padding: 10px 10px;">

<?php echo $output; ?>
</div>


            </div>
        </div>
        <!-- main content area end -->
        <!-- footer area start-->
        <?php $this->load->view('layout/event_admin/footer');?>
        <!-- footer area end-->
    </div>
    <!-- page container area end -->
    
    <?php $this->load->view('layout/event_admin/javascript');?>
    <?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script type="application/javascript">

  /*================================
    sidebar collapsing
    ==================================*/
    if($.cookie('name')=='sidebar')
        {
         $('.page-container').removeClass('sbar_collapsed');   
        }else{
   
	 $('.page-container').addClass('sbar_collapsed');
        }
	 $('.nav-btn').on('click', function() {
         
       // $.cookie('name', 'sidebar', { expires: 7, path: '/' });
          var act = $('.page-container').hasClass("sbar_collapsed");
         console.log(act);
         if(act==true)
             {
                 $.removeCookie('name', { path: '/' });
             }else{
                 $.cookie('name', 'sidebar', { expires: 7, path: '/' });
             }
        
    }); 

</script>
</body>

</html>
