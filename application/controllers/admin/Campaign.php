<?php if (!defined('BASEPATH')) {
	exit('no direct access script allowed');
}

class Campaign extends CI_Controller {

	public function __construct() 
	{

		parent::__construct();
		
		$this->load->helper('grocery_crud_helper');

        $this->load->library('Grocery_CRUD');
        $this->load->model('Login_auth_db');
		
	}
		  public function index()
    {
                
		try
		{
			$crud = _getGroceryCrudEnterprise();
			$crud->setTable("campaign");
			$crud->columns(['c_name','ad_format','pricing_model','camp_group']);
			$crud->displayAs('c_client_id', 'Client Name');
			$crud->displayAs('c_name', 'Campaign Name');
			
		
			if($this->session->userdata('level')!='Super'){ 
			 $where=array('cusID'=>$this->session->userdata('id'));
			 $wherecg=array('cg_client_id'=>$this->session->userdata('id'));
			 $crud->setRelation('c_client_id','client_details','company_name',$where);
			$crud->setRelation('camp_group','campaign_groups','cg_name',$wherecg);
			}else
			{
						$crud->setRelation('c_client_id','client_details','company_name');
			$crud->setRelation('camp_group','campaign_groups','cg_name');
			}
	
			$crud->setFieldUpload('creative', 'assets/uploads', base_url() . '/assets/uploads');
			$output = $crud->render();
			$data['title']="Campaign Management";
			$output->data = $data;
			_example_output($output);

		}catch(Exception $e)
		{
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}      
         
    }
	
		  public function groups()
    {
                
		try
		{
			$crud = _getGroceryCrudEnterprise();
			$crud->setTable("campaign_groups");
			$crud->columns(['cg_name','time_stamp','status']);
			
			
			if($this->session->userdata('level')!='Super'){ 
			 $where=array('cusID'=>$this->session->userdata('id'));
			 $crud->setRelation('cg_client_id','client_details','company_name',$where);
			}else{
			$crud->setRelation('cg_client_id','client_details','company_name');
			}
			$crud->displayAs('cg_name', 'Campaign Group Name');
			$crud->displayAs('cg_client_id', 'Client Name');
			$crud->displayAs('time_stamp', 'Date Added');
			$crud->callbackColumn('cg_name', function ($value, $row) {
     return "<a href='".admin_folder()."/view_campaign_groups/" . $row->cg_id."' target='blank'>$value</a>";
});
			$output = $crud->render();
			$data['title']="Client Groups Management";
			$output->data = $data;
			_example_output($output);

		}catch(Exception $e)
		{
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}      
         
    }
	}
	?>