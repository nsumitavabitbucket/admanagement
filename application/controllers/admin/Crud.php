<?php if(!defined('BASEPATH')) exit('no direct access script allowed');

class Crud extends CI_Controller {

	public function __construct() {
		
		parent::__construct(); 
		$this->load->library('grocery_CRUD');
		$this->load->helper('page_creator');
		 date_default_timezone_set('Asia/Kolkata');
		// to protect the controller to be accessed only by registered users
	   if(!$this->session->userdata('logged_in')){
			
			redirect('admin/login', 'refresh');
			 		
		}
       // $this->load->model('UserRegistration');

	}
		

	public function _example_output($output = null)
	{
		$this->load->view('admin/crud_base_view.php',(array)$output);
	}
	
	
		public function client()
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('client_info');
			$crud->set_subject('Client');
			$crud->columns('client_info_client_name','client_info_logo','client_info_company_name','client_info_email','client_info_mobile');
            $crud->display_as('client_info_client_name','Client');
			$crud->display_as('client_info_company_name','Company Name');
            $crud->set_field_upload('client_info_logo','uploads/client_logos');
			$crud->display_as('client_info_email','Email');
			$crud->display_as('client_info_mobile','Mobile');
            $crud->field_type('client_info_status','dropdown',array('0' => 'Active', '1' => 'Deactive'));
			$output = $crud->render();
			$data['title'] = "Client";
            $output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	

	public function email_template()
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('email_templates');
			$crud->set_subject('Email Template');
			$crud->set_js('assets/admin/js/mycustom.js');
			 $crud->field_type('status','dropdown',array('0' => 'Active', '1' => 'Deactive'));
			$output = $crud->render();
			$data['title'] = "Email Template";
            $output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

			public function events()
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('events');
			$crud->set_subject('Events');
            $crud->columns('event_name','event_start_date','event_end_date','event_status','event_location');
            $crud->callback_after_insert(array($this, '_create_corresponding_event_table'));
            $crud->set_relation('client_id','client_info','client_info_client_name');
            $crud->set_field_upload('logo','uploads/event_logos');
            $crud->field_type('event_status','dropdown',array('0' => 'Active', '1' => 'Deactive'));
			$crud->add_action('Sessions', '', 'admin/crud/setSessions','ui-icon-plus');
			$crud->add_action('Agenda', '', 'admin/crud/setDigitalAgenda','ui-icon-plus');
			$crud->add_action('Speaker', '', 'admin/crud/setSpeaker','ui-icon-plus');
            $crud->add_action('User', '', 'admin/crud/userForEvents','ui-icon-plus');
			$crud->unset_read();
			$crud->unset_clone();
			$output = $crud->render();
			$data['title'] = "Events ";
            $output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
    
		public function admin_users()
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('admin_users');
			$crud->set_subject('Admin Users');
			$crud->columns('name','username','password','level','status');
			$crud->field_type('level','dropdown',array('event', 'Super'));
			
			$output = $crud->render();
			$data['title'] = "Admin Users for Events";
            		$output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
   /* public function sessions()
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('day_section');
			$crud->set_subject('Sessions');
			$crud->display_as('day_sections','Sessions');
            $output = $crud->render();
			$data['title'] = "Sessions Master";
            $output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}*/
    public function setSessions($eventId)
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('event_day_details');
			$crud->set_subject('Event Days & Sessions');
			$finalArray=$this->getPossibleDays($eventId);
			$crud->field_type('day_list','dropdown',$finalArray );
			$crud->where('event_id',$eventId);
			$crud->field_type('event_id', 'hidden', $eventId);
            //$crud->columns('event_name','event_start_date','event_end_date','event_status','event_location');
           // $crud->callback_after_insert(array($this, '_create_corresponding_event_table'));
            //$crud->set_relation('event_id','events','event_name',array('idevents' => $eventId));
         // $crud->set_relation('day_id','event_day_details','day_list');
          //  $crud->set_relation('day_id','day_master','day_list');
//            

// 
		// foreach($result->result() as $row) :
		// $string= array($row->idday_master => $row->day_list);
	 // $crud->field_type('day_id','dropdown',$string);
		//  endforeach;
           // $crud->field_type('event_status','dropdown',array('0' => 'Active', '1' => 'Deactive'));
			$crud->unset_read();
			$crud->unset_clone();
			$output = $crud->render();
			$data['title'] = "Event Days & Sessions ";
            $output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function setDigitalAgenda($eventId)
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');

			$crud->set_table('event_agenda');
			$crud->set_subject('Event Daywise Agenda');
			$finalArray=$this->getPossibleDays($eventId);	
			$crud->field_type('event_date','dropdown',$finalArray );
			$crud->set_relation('speaker_fid','event_speakers','name',array('event_id' => $eventId));
			$crud->display_as('speaker_fid','Speaker');
			$crud->where('eventids',$eventId);
			$crud->field_type('eventids', 'hidden', $eventId);
			$crud->set_js('assets/admin/js/mycustom.js');
			$crud->field_type('created', 'hidden', date('Y-m-d H:i:s'));
			$crud->unset_read();
			$crud->unset_clone();
			$output = $crud->render();
			$data['title'] = "Event day wise agenda";
            $output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function setSpeaker($eventId)
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');

			$crud->set_table('event_speakers');
			$crud->set_subject('Event Speaker');
			$crud->where('event_id',$eventId);
			$crud->field_type('event_id', 'hidden', $eventId);
			$crud->field_type('created_d', 'hidden', date('Y-m-d H:i:s'));
			$crud->unset_read();
			$crud->unset_clone();
			$output = $crud->render();
			$data['title'] = "Event Speaker";
            $output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function getPossibleDays($eventID)
	{
		$query = $this->db->query("Select * from events where idevents= ".$eventID);
		//$query = $this->db->get();
		$ret = $query->row();
		//Declare two dates 
$Date1 =str_replace('/', '-', $ret->event_start_date);  
$Date2 =str_replace('/', '-',$ret->event_end_date);

//Declare an empty array 
$array = array(); 
  
// Use strtotime function 
$Variable1 = strtotime($Date1); 
$Variable2 = strtotime($Date2); 
  
// Use for loop to store dates into array 
// 86400 sec = 24 hrs = 60*60*24 = 1 day 
for ($currentDate = $Variable1; $currentDate <= $Variable2;  
                                $currentDate += (86400)) { 
                                      
$Store = date('Y-m-d', $currentDate); 
$array[] = $Store; 


}

$finalArray = array();
foreach($array as $value)
{
	$finalArray[$value]=$value;
}
return $finalArray;

	}
	
    public function _create_corresponding_event_table($row,$primary_key)
    {
    
        $this->load->dbforge();
       // switch over to Library DB
$this->db->query('use events');

// define table fields
$fields = array(
        'idregistrations' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
        ), 
        'client_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
        ),'event_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
        ),'day_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
        ),'shift_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
        ),
        'full_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
        ),'company_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
        ),'mob_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
        ),'pincode' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
        ),
        'company_type' => array(
                'type' =>'VARCHAR',
                'constraint' => '100'
        ),'industry' => array(
                'type' =>'VARCHAR',
                'constraint' => '100'
        ),'designation' => array(
                'type' =>'VARCHAR',
                'constraint' => '100'
        ),'email' => array(
                'type' =>'VARCHAR',
                'constraint' => '100'
        ),'city' => array(
                'type' =>'VARCHAR',
                'constraint' => '100'
        ),'job_role' => array(
                'type' =>'VARCHAR',
                'constraint' => '100'
        ),'year' => array(
                'type' =>'VARCHAR',
                'constraint' => '10'
        ),'user_type' => array(
                'type' =>'VARCHAR',
                'constraint' => '100'
        ),'registration_type' => array(
                'type' =>'VARCHAR',
                'constraint' => '100',
            'comment'=>'1=onsite,0=pre_register'
        ),'company_size' => array(
                'type' =>'VARCHAR',
                'constraint' => '100'
        ),'created' => array(
                'type' =>'DATETIME'
        ),
        'registration_status' => array(
                'type' => 'INT',
                'null' => TRUE,
            'comment'=>'1=Confirmed 2=Pending'
        )
);

$this->dbforge->add_field($fields);

// define primary key
$this->dbforge->add_key('idregistrations', TRUE);

$tablename=date('Y').'_'.$row['url'].'_registrations';
// create table
$this->dbforge->create_table($tablename);


create_new_page($tablename, $row['url'], $row['url']);


    }

    

	public function userForEvents($eventID)
	{
		
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('admin_users');
			$crud->set_subject('Admin Users');
			$crud->columns('name','username','password','level','status');
					
			$crud->set_relation('event','events','event_name',array('idevents'=>$eventID));
			$crud->field_type('level','dropdown',array('event', 'Super'));
			$crud->where('event',$eventID);
          //  if($this->session->userdata('user_data_session')['level']!='Super'){
		//	$crud->unset_delete();
			///$crud->unset_edit();
			// }
			$crud->unset_print();
            $crud->unset_clone();
            //$crud->unset_add();
         
			$output = $crud->render();
			$data['title'] = "Admin Users for Events";
            $output = array_merge($data,(array)$output);
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	
}