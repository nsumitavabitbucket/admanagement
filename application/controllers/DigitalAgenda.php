
<?php defined('BASEPATH') OR exit('No direct script access allowed');
class DigitalAgenda extends CI_Controller  {
	public function __construct()
    {
        $data=array();
		parent::__construct();
        $this->load->model('digitalAgendaModel');
        date_default_timezone_set('Asia/Kolkata');
    }
       
     public function index()
    {
         $data['clients']=$this->digitalAgendaModel->getEventClients();
         $this->load->view('digital_agenda/digital_agenda_clients',$data);
         
    }
    // List the events as per client ID
    
       public function events($clientid)
    {
        $data['client_name']=getClientDetails(array('client_info_client_name'),$clientid)->client_info_client_name;
         $data['clientsEvents']=$this->digitalAgendaModel->getClientsEvents($clientid);
         $this->load->view('digital_agenda/digital_agenda_events',$data);
         
    } 
    
    public function rooms($eventID,$eventUrl)
    {
         $data['event_name']=getEvent('id',$eventUrl)['event_name'];
         $data['roomsEvents']=$this->digitalAgendaModel->getRoomsEvents($eventID);
         $this->load->view('digital_agenda/event_rooms',$data);
         
    }  
    
    public function sessions($eventID,$roomID)
    {
        $data=array();
         //$data['sessions']=$this->digitalAgendaModel->getSessions($eventID,$roomID);
         $this->load->view('digital_agenda/sessions_display',$data);
         
    } 
    
    public function sessionsdisplay($eventurl,$roomID)
    {
        $eventID=getEvent('id',$eventurl)['idevents'];
    	$time=date("H:i:s");
    	$today=date('Y-m-d');
    	$data['session']=$this->digitalAgendaModel->getNowAndNextSession($eventID,$roomID,$time,$today);
    	$i=0;
    	$result='';
foreach($data['session'] as $value):
       $result.= '<div class="row">
			<div class="col-lg-12 col-xs-12">
				<div class="next-list">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td width="3%" class="next-now" align="center">';
								if($i==0)
								{
									$result.= 'N<br>o<br>w<br>';
								}else
								{
									$result.= 'N<br>e<br>x<br>t';
								}
							$result.= '</td>
							<td width="20%" class="time" align="center" valign="top">'.$value->start_time.'-'.$value->end_time.'</td>
							<td width="77%" class="speakers-info" align="left" valign="top">
								<p>'.$value->session.'</p>
								<div>
									<span class="speakers-topic">Dan Liberman</span>
									<span class="speakers-name"> (IT SmartThings)</span>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>	
		</div>';
		$i++;
endforeach;
echo $result;
        exit;
         
    }
    
    
    }
    ?>