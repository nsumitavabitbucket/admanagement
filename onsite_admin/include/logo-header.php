<!-- header area start -->
<div class="header-area">
	<div class="row align-items-center">
		<!-- nav and search button -->
		<div class="col-md-12 col-sm-12 clearfix">
			<div class="nav-btn pull-left">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="logo pull-left">
				<a href="index.php"><img src="assets/images/logo.png" alt="logo"></a>
			</div>

		   <ul class="notification-area pull-right">
				<li id="full-view"><i class="ti-fullscreen"></i></li>
				<li id="full-view-exit"><i class="ti-zoom-out"></i></li>
			</ul>
			<div class="search-box pull-right">
				<form action="#">
					<input type="text" name="search" placeholder="Search..." required>
					<i class="ti-search"></i>
				</form>
			</div>



		</div>
	</div>
</div>
<!-- header area end -->